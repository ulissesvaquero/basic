Yii 2 PROJETO BASICO COM USER E RIGHTS PT-BR
============================
 Projeto básico com user e rights já integrados, não esquecer de rodar os seguintes
 migrates.
 
 Rodar o comando
  
 composer update
 
 E os migrations:
 
yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations
 
yii migrate --migrationPath=@yii/rbac/migrations
 

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      messages/			 tradução
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources


Configuração
-------------

### Database

Editar o arquivo `config/db.php`, por exemplo:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```


