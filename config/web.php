<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
	'language' => 'pt-BR',
    'bootstrap' => ['log'],
	'modules' => [
		'user' => [
			'class' => 'dektrium\user\Module',
			'enableConfirmation' => false
		],
		'admin' => [
				'class' => 'mdm\admin\Module',
				'controllerMap' =>[
						'assignment' =>[
								'class' => 'mdm\admin\controllers\AssignmentController',
								'userClassName' => 'dektrium\user\models\User',
								'idField' => 'id',
								'usernameField' => 'username',
					]
						],
				'layout' => 'left-menu',
				'mainLayout' => '@app/views/layouts/main.php',
		],
	],
    'components' => [
    	'authManager' => [
    			'class' => 'yii\rbac\DbManager',
    	],
    	'i18n' => [
    			'translations' => [
    					'user' => [
    							'class' => 'yii\i18n\PhpMessageSource',
    							'basePath' => '@app/messages',
    					],
    			],
    	],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'YkhumST11LyzhJx2Aql34-OkVKTOb4x6',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
    		'identityClass' => 'dektrium\user\models\User'
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
		
	/*
	 * Configuração do ACL, será essa classe que irá
	 * permitir ou negar.
	 *
	 */
	
	'as access' =>[
			'class' => 'mdm\admin\components\AccessControl',
			'allowActions' => [
				'user/security/login',
				'site/error',
				'user/security/logout'
		]
	]
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
